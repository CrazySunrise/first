import java.util.Comparator;

public class Student implements Comparable<Student> {
    String FIO;
    int group;
    double point;
    public Student(String FIO, int group, double point){
        this.FIO = FIO;
        this.group = group;
        this.point = point;
    }

    public double getPoint() {
        return point;
    }

    public int compareTo(Student emp) {
        return (int) (this.point - emp.point);
    }

    public void printer(){
        System.out.printf("\nФИО: %s\nГруппа: %d\nСредний балл: %f\n",FIO ,group, point);
    }

}
