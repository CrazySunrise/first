import java.util.*;
import java.util.Collections;

public class main {

    public static void swap(List<Student> list, int i, int j)
    {
        Student tmp = list.get(i);
        list.set(i,list.get(j));
        list.set(j,tmp);
    }

    public static String Vvod(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }


    public static void main(String[] args) {
        double point = 0;
        List<Student> student = new ArrayList<>();
        System.out.println("Добавление");
        for (int i = 0;i<4;i++){
            String FIO = Vvod("Фамилия и инициалы");
            int group = Integer.valueOf(Vvod("Группа"));
            System.out.println("Успеваемость");
            for (int j = 0;j<5;j++){
                Scanner scanner = new Scanner(System.in);
                int b = scanner.nextInt();
                point = point + b;
            }
            point = point/5;

            student.add(new Student(FIO, group, point ));
        }
        System.out.println("Сортировка");
        Collections.sort(student);
        for (int i = 0; i< 10; i++){
            student.get(i).printer();
        }
    }
}
